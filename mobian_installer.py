# #!/usr/bin/env python3

from argparse import ArgumentParser
import subprocess
from urllib.request import urlopen
from pathlib import Path
from tempfile import mkdtemp
import datetime
import logging
import os
import sys
import tarfile
import time

# Import local modules from the temporary directory where the archive
# has been extracted on the mobile device, and where this script is
# being executed.
sys.path.insert(0, "py")
sys.path.insert(0, "libparted2")

import parted

log = None
conf = None
BASEURL = "http://mobian.debian.net"

def parse_args():
    ap = ArgumentParser()
    ap.add_argument(
        "--root-url",
        help="Root filesystem tarball URL",
        default=f"{BASEURL}/mi/pp-root.tar.xz",
    )
    ap.add_argument(
        "--boot-url",
        help="Boot filesystem tarball URL",
        default=f"{BASEURL}/mi/pp-boot.tar.xz",
    )
    return ap.parse_args()


def setup_logging(logfn: str):
    log = logging.getLogger("Mobian installer")
    log.setLevel(logging.DEBUG)

    fh = logging.FileHandler(logfn, mode="w")
    fh.setLevel(logging.DEBUG)
    log.addHandler(fh)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    log.addHandler(ch)
    return log


def list_devices():
    """Return [(device_path, size_mb, model), ... ]"""
    return sorted((d.path, d.getSize(), d.model) for d in parted.getAllDevices())


def run(cmd, timeout=5):
    cmd = " ".join(str(x) for x in cmd)
    log.info(f"Running {cmd}")
    try:
        output = subprocess.check_output(
            cmd,
            stderr=subprocess.STDOUT,
            shell=True,
            timeout=timeout,
            universal_newlines=True,
        )
    except subprocess.CalledProcessError as e:
        log.error(f"Error {e.returncode} {e.output} {e.stderr}")
        raise

    # log.info("Output: \n{}\n".format(output))
    return output


def umount(dirname):
    run(("umount", dirname), timeout=120)


def download_and_extract_fs(url: str, mountdir: Path, total_bytes: int):
    assert mountdir.is_absolute()
    assert mountdir.is_dir()
    log.info(f"Fetching tarball from {url}")
    r = urlopen(url)
    log.info("Extracting tarball to %s", mountdir)
    if url.endswith(".gz"):
        tar = tarfile.open(fileobj=r, mode="r|gz")
    elif url.endswith(".xz"):
        tar = tarfile.open(fileobj=r, mode="r|xz")

    written_bytes = 0
    start_time = last_update_time = time.monotonic()
    for i in tar:
        desc = i.path
        if i.isdev():
            desc += " (device)"
        if i.isdir():
            desc += " (directory)"
        if i.issym():
            desc += " (symlink)"
        if os.path.isabs(i.path):
            log.error("Unexpected abspath %s", i.path)
            continue
        dest = mountdir.joinpath(i.path)
        if mountdir not in dest.parents:
            # TODO: filter out legitimate paths
            # log.error("Unexpected traversal %s", i.path)
            continue

        try:
            tar.extract(i, path=mountdir)
        except Exception as e:
            if i.path.startswith("./dev/"):
                continue
            msg = f"{desc} {e}"
            log.error(msg)
            continue

        written_bytes += i.size
        t = time.monotonic()
        if t > last_update_time + 5:
            elapsed = t - start_time
            eta = elapsed / float(written_bytes) * total_bytes - elapsed
            eta = str(datetime.timedelta(seconds=int(eta)))
            log.info(f"ETA: {eta}")
            last_update_time = t

    log.info("Extraction completed")


def setup_etc_fstab(boot_partition_path, root_partition_path, tmpdir):
    log.info("Configuring /etc/fstab")
    fstab = tmpdir / "etc/fstab"
    fstab.write_text(
        f"""
{boot_partition_path}       /boot   ext4    defaults        0       2
{root_partition_path}       /       f2fs    defaults        0       1
"""
    )


def deploy_root_partition(root_partition_path):
    log.info("Formatting root partition")
    cmd = ("./sbin/mkfs.f2fs", "-f", "-O", "encrypt", root_partition_path)
    run(cmd, timeout=60)
    # TODO compression

    tmpdir = Path(mkdtemp(prefix="mobian"))

    log.info("Mounting root partition")
    run(("mount", root_partition_path, tmpdir))
    run(("chmod", "a+rw", "-R", tmpdir))

    # Copy root FS into tmp dir

    try:
        tmpdir.mkdir(parents=True, exist_ok=True)
        download_and_extract_fs(conf.root_url, tmpdir, 2552496000)
    except Exception as e:
        log.error(e)
        umount(tmpdir)
        sys.exit(1)

    log.info("Unmounting root partition")
    umount(tmpdir)


def deploy_boot_partition(boot_partition_path):
    log.info("Formatting boot partition")
    cmd = ("mkfs.ext4", "-F", boot_partition_path)
    run(cmd, timeout=60)

    tmpdir = Path(mkdtemp(prefix="mobian"))

    log.info("Mounting boot partition")
    run(("mount", boot_partition_path, tmpdir))
    run(("chmod", "a+rw", "-R", tmpdir))
    try:
        download_and_extract_fs(conf.boot_url, tmpdir, 26119)
    except Exception as e:
        log.error(e)
        umount(tmpdir)
        sys.exit(1)

    log.info("Unmounting boot partition")
    umount(tmpdir)


def pick(li, q="Please choose one option:"):
    """Ask user to pick an element in a list"""
    while True:
        log.info(q)
        for n, e in enumerate(li):
            if isinstance(e, list) or isinstance(e, tuple):
                e = " ".join(e)
            log.info(f"{n} - {e}")
        choice = input("> ")
        try:
            choice = int(choice)
            if choice >= 0 and choice < len(li):
                return li[choice]
        except ValueError:
            pass


def check_required_dependencies():
    pass


def pick_target_device():
    li = []
    for devpath, size_mb, model in list_devices():
        if "boot" in devpath or "zram" in devpath:
            continue
        if size_mb > 4000:
            size = f"{int(size_mb/1000)} GB"
        else:
            size = f"{int(size_mb)} MB"
        li.append((devpath, size, model))
    return pick(li, "Pick a target device CAREFULLY.")[0]


def setup_u_boot(target_device, tmpdir):
    devpath = os.path.join("/dev", target_device)
    ipath = tmpdir / "usr/bin/u-boot-install-sunxi64"
    run((ipath, devpath))


def show_partitions_ask_confirmation(target_device):
    log.info("Checking device...")
    dev = parted.getDevice(target_device)
    disk = parted.newDisk(dev)
    disk.check()
    log.info("Partitions found:")
    for p in disk.partitions:
        in_use = "IN USE" if p.busy else ""
        size_mb = int(p.getSize())
        log.info(f"  {p.path} {size_mb}MB {in_use}")

    ans = input("Destroy all partitions [y/n]? ")
    if ans != "y":
        log.info("Exiting")
        sys.exit(0)


def create_partitions_parted(target_device):
    # Currently unused
    log.info("Partitioning...")
    assert target_device.startswith("/dev/")
    dev = parted.getDevice(target_device)
    disk = parted.freshDisk(dev, "msdos")

    # boot
    boot_len = parted.sizeToSectors(120, "MiB", dev.sectorSize)
    geo = parted.Geometry(device=dev, start=1, end=boot_len)
    filesystem = parted.FileSystem(type="ext3", geometry=geo)
    partition = parted.Partition(
        disk=disk, type=parted.PARTITION_NORMAL, fs=filesystem, geometry=geo
    )
    disk.addPartition(partition=partition, constraint=dev.optimalAlignedConstraint)
    partition.setFlag(parted.PARTITION_BOOT)

    boot_partition_path = partition.path

    # root
    geo = parted.Geometry(device=dev, start=boot_len, end=dev.getLength())
    filesystem = parted.FileSystem(type="ext4", geometry=geo)
    partition = parted.Partition(
        disk=disk, type=parted.PARTITION_NORMAL, fs=filesystem, geometry=geo
    )
    disk.addPartition(partition=partition, constraint=dev.optimalAlignedConstraint)
    root_partition_path = partition.path

    disk.commit()
    # run(("partprobe",))
    return boot_partition_path, root_partition_path


def create_partitions(target_device):
    log.info("Partitioning...")
    assert target_device.startswith("/dev/")
    cnf = "/tmp/sfdisk.conf"
    Path(cnf).write_text(
        f"""
label: dos
label-id: 0xc0ffeeee
device: {target_device}
unit: sectors
sector-size: 512

{target_device}p1 : start=        2048, size=      245760, type=83
{target_device}p2 : start=      247808, size=    30537728, type=83
"""
    )
    cmd = ("sfdisk", target_device, "<", cnf)
    run(cmd, timeout=20)
    run(("partprobe",))
    return f"{target_device}p1", f"{target_device}p2"


def main():
    global log, conf
    logfn = os.path.join(os.environ["USER_PWD"], "mobian_installer.log")
    log = setup_logging(logfn)
    conf = parse_args()
    log.debug("Running from %s", os.getcwd())
    #log.debug(os.listdir(os.getcwd()))
    #cmd = ("./sbin/mkfs.f2fs", "-h")
    #run(cmd, timeout=60)
    log.info("")
    log.info("Welcome to the Mobian installer 0.1")
    log.info(f"Logging to {logfn}")

    # check_required_dependencies()
    target_device = pick_target_device()
    assert target_device.startswith("/dev/")
    show_partitions_ask_confirmation(target_device)
    boot_partition_path, root_partition_path = create_partitions(target_device)
    deploy_boot_partition(boot_partition_path)
    deploy_root_partition(root_partition_path)

    tmpdir = Path(mkdtemp(prefix="mobian"))
    run(("mount", root_partition_path, tmpdir))

    setup_etc_fstab(boot_partition_path, root_partition_path, tmpdir)

    setup_u_boot(target_device, tmpdir)

    log.info("Unmounting root partition")
    umount(tmpdir)


main()
